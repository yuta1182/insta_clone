class PasswordResetsController < ApplicationController
  def new
  end

  def edit
  end
  
  def update
    user = current_user
    password = params[:password_resets][:password]
    reset = params[:password_resets][:reset]
    reset_confirmation = params[:password_resets][:reset_confirmation]
    if password.empty? || reset.empty? || reset_confirmation.empty?
      flash.now[:gender] = "password or new_password not alive"
      render 'edit'
    elsif !(reset == reset_confirmation)
      flash.now[:gender] = "new_password not match confirmation"
      render 'edit'
    elsif user && user.authenticate(params[:password_resets][:password])
      user.update(password: params[:password_resets][:reset],password_confirmation: params[:password_resets][:reset_confirmation])
      flash[:success] = "Password has been reset."
      redirect_to user
    else
      flash.now[:gender] = "password missmatch"
      render 'edit'
    end
  end
  
  private
  
  def password_params
    params.require(:password_resets).permit(:password, :reset,
                                            :reset_confirmation)
  end
end
